import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;

import static org.junit.Assert.assertEquals;

public class ParsXMLTest {
    private ParsXML parsXML;
    private List<Address> list;
    private Map<Address, Integer> map;
    private Set<String> set;
    private List<City> citiesList;
    private City saranskCity;
    private City sevastopolCity;
    private ByteArrayOutputStream output;
    private String repeatTest;

    @Before
    public void init() {
        parsXML = new ParsXML();
        list = new ArrayList<>();
        list.add(new Address("�������", "���������", 60, 3));
        list.add(new Address("�������", "���������", 58, 3));
        list.add(new Address("�������", "���������", 140, 1));
        list.add(new Address("�������", "���������", 58, 3));
        list.add(new Address("�����������", "��. ������������", 116, 4));
        map = new HashMap<>();
        set = new TreeSet<>();
        citiesList = new ArrayList<>();
        saranskCity = new City("�������", 1, 0, 2, 0, 0);
        sevastopolCity = new City("�����������", 0, 0, 0, 1, 0);
        output = new ByteArrayOutputStream();
        repeatTest = "Repeating element: {city='�������', street='���������', house=58, floor=3}, number of repetitions 2\r\n";
    }

        @Before
        public void setUpStreams(){
            System.setOut(new PrintStream(output));
    }

    @Test
    public void shouldAddToMapFromListWishCount() {
        parsXML.addToMapFromListWithCount(list, map);
        assertEquals(map.size(), 4);

        map.clear();
    }


    @Test
    public void shouldAddToSetCitiesTrue() {
        parsXML.addToSetCities(list, set);
        assertEquals(set.size(), 2);

        set.clear();
    }

    @Test
    public void shouldCountFloorInCitiesTrue() {
        parsXML.addToSetCities(list, set);
        parsXML.countFloorInCities(set, list, citiesList);

        assertEquals(citiesList.get(0), saranskCity);
        assertEquals(citiesList.get(1), sevastopolCity);

        list.clear();
        set.clear();
        citiesList.clear();
    }

    @Test
    public void shouldPrintDuplicatesTrue() {
        parsXML.addToMapFromListWithCount(list, map);
        parsXML.printDuplicates(map);
        assertEquals(repeatTest, output.toString());

        list.clear();
        map.clear();
    }

    @After
    public void cleanUpStreams() {
        System.setOut(null);
    }
}
