import java.util.Objects;

public class City {
    private String name;
    private int countOneFloor;
    private int countTwoFloor;
    private int countThreeFloor;
    private int countFourFloor;
    private int countFiveFloor;

    public City(String name, int countOneFloor, int countTwoFloor, int countThreeFloor, int countFourFloor, int countFiveFloor) {
        this.name = name;
        this.countOneFloor = countOneFloor;
        this.countTwoFloor = countTwoFloor;
        this.countThreeFloor = countThreeFloor;
        this.countFourFloor = countFourFloor;
        this.countFiveFloor = countFiveFloor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCountOneFloor() {
        return countOneFloor;
    }

    public void setCountOneFloor(int countOneFloor) {
        this.countOneFloor = countOneFloor;
    }

    public int getCountTwoFloor() {
        return countTwoFloor;
    }

    public void setCountTwoFloor(int countTwoFloor) {
        this.countTwoFloor = countTwoFloor;
    }

    public int getCountThreeFloor() {
        return countThreeFloor;
    }

    public void setCountThreeFloor(int countThreeFloor) {
        this.countThreeFloor = countThreeFloor;
    }

    public int getCountFourFloor() {
        return countFourFloor;
    }

    public void setCountFourFloor(int countFourFloor) {
        this.countFourFloor = countFourFloor;
    }

    public int getCountFiveFloor() {
        return countFiveFloor;
    }

    public void setCountFiveFloor(int countFiveFloor) {
        this.countFiveFloor = countFiveFloor;
    }

    @Override
    public String toString() {
        return "{" +
                "name='" + name + '\'' +
                ", одноэтажных домов = " + countOneFloor +
                ", двухэтажных домов = " + countTwoFloor +
                ", трехэтаждных домов = " + countThreeFloor +
                ", четырехэтажных домов =" + countFourFloor +
                ", пятиэтажных домов = " + countFiveFloor +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City city = (City) o;
        return countOneFloor == city.countOneFloor &&
                countTwoFloor == city.countTwoFloor &&
                countThreeFloor == city.countThreeFloor &&
                countFourFloor == city.countFourFloor &&
                countFiveFloor == city.countFiveFloor &&
                Objects.equals(name, city.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, countOneFloor, countTwoFloor, countThreeFloor, countFourFloor, countFiveFloor);
    }
}
