import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.*;
import java.util.*;

public class ParsXML extends Thread{
    private static List<Address> addressList = new ArrayList<>();
    private static Map<Address, Integer> map = new HashMap<>();
    private static Set<String> cities = new TreeSet<>();
    private static List<City> citiesList = new ArrayList<>();

    public static void main(String[] args) throws IOException, SAXException, ParserConfigurationException {
        ParsXML myPars = new ParsXML();

        parseXML(args[0]);
        myPars.addToMapFromListWithCount(addressList, map);
        myPars.printDuplicates(map);
        myPars.addToSetCities(addressList, cities);
        myPars.countFloorInCities(cities, addressList, citiesList);
        System.out.println(citiesList);
    }
    private static void parseXML(String s) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser = factory.newSAXParser();

        XMLHandler handler = new XMLHandler();

        try(BufferedReader reader = new BufferedReader
                (new InputStreamReader(new FileInputStream(s))))
        {
            InputSource is = new InputSource(reader);
            is.setEncoding("UTF-8");
            parser.parse(is, handler);}
    }
    public void addToMapFromListWithCount(List<Address> list, Map<Address, Integer> map) {
        list.forEach((element) -> {
            if (map.containsKey(element)) {
                map.put(element, map.get(element) + 1);
            } else
                map.put(element, 1);
        });
    }

    public void printDuplicates(Map<Address, Integer> map) {
        map.forEach((k, v) -> {
            if(v > 1)
                System.out.println("Repeating element: " + k + ", number of repetitions " + v);
        });
    }

    public void addToSetCities(List<Address> list, Set<String> cities) {
        list.forEach((element) -> cities.add(element.getCity()));
    }

    public void countFloorInCities(Set<String> cities, List<Address> addressList, List<City> citiesList) {
        int one = 0;
        int two = 0;
        int three = 0;
        int four = 0;
        int five = 0;

        Set<Address> addressSet = new HashSet<>(addressList);
        for(String city : cities) {
            for (Address address : addressSet) {
                if (city.equals(address.getCity())) {
                    if (address.getFloor() == 1) one++;
                    else if (address.getFloor() == 2) two++;
                    else if (address.getFloor() == 3) three++;
                    else if (address.getFloor() == 4) four++;
                    else if (address.getFloor() == 5) five++;
                }
            }
            citiesList.add(new City(city, one, two, three, four, five));
            one = 0;
            two = 0;
            three = 0;
            four = 0;
            five = 0;
        }
    }

    private static class XMLHandler extends DefaultHandler {

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) {
            if(qName.equals("item")){
                String city = attributes.getValue("city");
                String street = attributes.getValue("street");
                int house = Integer.parseInt(attributes.getValue("house"));
                int floor = Integer.parseInt(attributes.getValue("floor"));
                addressList.add(new Address(city, street, house, floor));
            }
        }
    }
}